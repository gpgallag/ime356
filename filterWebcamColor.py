import time
import cv2
import numpy as np
from stackImages import StackImages


def Empty(a):
    '''
    @brief An Empty function used to create the taskbar.
    '''
    pass

cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
cap.set(3,640)
cap.set(4,480)
frameRate = 60

cv2.namedWindow('TrackBars')
cv2.resizeWindow('TrackBars', 640, 280)

# Create Trackbar sliders used to control the thresholds of Hue, Saturation, and Value.
cv2.createTrackbar('Hue Min', 'TrackBars', 0,   180, Empty)
cv2.createTrackbar('Hue Max', 'TrackBars', 180, 180, Empty)
cv2.createTrackbar('Sat Min', 'TrackBars', 0, 256, Empty)
cv2.createTrackbar('Sat Max', 'TrackBars', 256, 256, Empty)
cv2.createTrackbar('Val Min', 'TrackBars', 0,   256, Empty)
cv2.createTrackbar('Val Max', 'TrackBars', 256,   256, Empty)


while True:
    ## Taking one frame from the webcam
    success, img = cap.read()
    
    ## The image file converted to HSV.
    imgHSV = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)    
    ## The minimum hue threshold.
    h_min = cv2.getTrackbarPos('Hue Min', 'TrackBars')    
    ## The maximim hue threshold.
    h_max = cv2.getTrackbarPos('Hue Max', 'TrackBars')    
    ## The minimum saturation threshold.
    s_min = cv2.getTrackbarPos('Sat Min', 'TrackBars')    
    ## The maximum saturation threshold.
    s_max = cv2.getTrackbarPos('Sat Max', 'TrackBars')    
    ## The minimum value threshold.
    v_min = cv2.getTrackbarPos('Val Min', 'TrackBars')
    ## The maximum value threshold.
    v_max = cv2.getTrackbarPos('Val Max', 'TrackBars')
        
    
    ## A list containing the lower bound threshold values.
    lowerb = np.array([h_min, s_min, v_min])
    ## A list containing the upper bound threshold values.
    upperb = np.array([h_max, s_max, v_max])
    ## The binary image file masked by the threshold values
    mask = cv2.inRange(img, lowerb, upperb)
    
    ## The original BGR image file overlapped in bitwise fashion with the masked image.
    imgResult = cv2.bitwise_and(img, img, mask=mask)    
    ## The image file containing the original, masked, and bitwise masked/original image.
    imgStack = StackImages(0.8, ([img, mask, imgResult]))    
    # Show the original, masked, and bitwise masked image as one image.
    cv2.imshow('Legos', imgStack)
    
    
    ## The key inputted by the user
    key = cv2.waitKeyEx(1)
    # User presses any key besides 0.
    if key != -1:
        # Close all windows and exit the file loop.
        cv2.destroyAllWindows()
        break
   
    else:
        time.sleep(1/frameRate)
