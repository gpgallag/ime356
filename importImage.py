
import cv2 # https://stackoverflow.com/questions/10417108/what-is-different-between-all-these-opencv-python-interfaces

print("Package Imported")

img = cv2.imread("Resources/donuts.jpg")
cv2.imshow("Output",img)


# If any button is pressed, destroy all windows and exit program
cv2.waitKey(0)
cv2.destroyAllWindows()