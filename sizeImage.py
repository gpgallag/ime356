import cv2 ## Import OpenCV package

## The specified image path.
path = 'Resources/donuts.jpg'

## The original image.
img = cv2.imread(path)

## The size, in pixels, of the original image.
imgSize = img.shape
print(imgSize)

cv2.imshow('Original Image', img)

## The original image, shrunk to 525x350
imgResize = cv2.resize(img, (525, 350))
cv2.imshow('Shrinked Image', imgResize)

## The original image, cropped to 525x350
imgCrop = img[0:525, 0:350]
cv2.imshow('Cropped Image', imgCrop)

# If any button is pressed, destroy all windows and exit program
cv2.waitKey(0)
cv2.destroyAllWindows()