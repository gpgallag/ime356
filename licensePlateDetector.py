import cv2
import time
 
## The width of the camera frame 
frameWidth = 640

## The height of the camera frame
frameHeight = 480

## The path of the Haar Cascade files.
nPlateCascade = cv2.CascadeClassifier("Resources/haarcascade_russian_plate_number.xml")

# Thresholding
minArea = 200
color = (255,0,255)
 
## The webcam object
cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
cap.set(3, frameWidth)  # Webcam width
cap.set(4, frameHeight) # Webcam height
cap.set(10,150)         # Webcam exposure

## The index number of the license plate scanned.
count = 0

## Framerate of the webcam/processor
framerate = 30
 
while True:
    success, img = cap.read()
    
    ## Grascaled webcam image.
    imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    
    ## Plate cascade object.
    numberPlates = nPlateCascade.detectMultiScale(imgGray, 1.1, 10)
    for (x, y, w, h) in numberPlates:
        # Calculated area of the detected plate.
        area = w*h
        
        # Check against threshold value
        if area > minArea:
            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 255), 2) # Place a purple rectangle around the license plate
            cv2.putText(img,"Number Plate",(x,y-5), cv2.FONT_HERSHEY_COMPLEX_SMALL,1,color,2) # Put identifier around box
            ## A zoomed-in image of the license plate.
            imgRoi = img[y:y+h,x:x+w]
            cv2.imshow("ROI", imgRoi)
    # Show the entire webcame image        
    cv2.imshow("Result", img)
     
    key = cv2.waitKey(1)
    # If the 's' key is pressed, save the license plate.
    if key and 0xFF == ord('s'):
        cv2.imwrite("Resources/Scanned/NoPlate_"+str(count)+".jpg",imgRoi)
        cv2.rectangle(img,(0,200),(640,300),(0,255,0),cv2.FILLED)
        cv2.putText(img,"Scan Saved",(150,265),cv2.FONT_HERSHEY_DUPLEX, 2,(0,0,255),2)
        cv2.imshow("Result",img)
        cv2.waitKey(500)
        count +=1
        
    elif key != -1:
        cv2.destroyAllWindows()
        break
    
    else:
        time.sleep(1/framerate)