import time
import cv2 ## Import OpenCV package
print('Starting')
## Initialize 'video' as the webcam
#  use '0' as default for a single connected camera.
cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)

# Set the size of the camera output
# set(3=width, 640=#pixels)
# 'default'
cap.set(3,640)
print('Width set')
# Set the length of the camera output
# (4=length, 480=#pixels)
# 'default'
cap.set(4,480)
print('Length set')


frameRate = 60

while True:
    success, img = cap.read() # Reads the next frame of the video and stores it as 'img'
    cv2.imshow("Webcam",img) # Shows the next frame of the video, 'img', and displays in new tab, 'Webcam'.
    
    # A 'break' statement. If the user presses 'q', exit the program.
    if cv2.waitKey(1) != -1:
        cv2.destroyAllWindows()
        break
    
    time.sleep(1/frameRate) # Reduces the amount of processing.