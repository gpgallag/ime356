import cv2
import numpy as np

if __name__ == '__main__':
    ## The specified image path.
    path = 'Resources/cards.jpg'
    
    ## The original image.
    img = cv2.imread(path)
        
    width,height  = 400, 250
    
    ## The coordinates of the image section to be warped.
    pts1 = np.float32([[531,216], [867,222], [540,419], [908,420]])
    
    ## The coordinates of the mapped output image.
    pts2 = np.float32([[0,0], [width,0], [0,height], [width,height]])
    
    ## The transformation matrix used to map pts1 onto pts2
    transMatrix = cv2.getPerspectiveTransform(pts1, pts2)
    
    ## The warped image.
    imgOut = cv2.warpPerspective(img, transMatrix, (width, height))
    
    
    # Show the original and warped images.
    cv2.imshow('Original', img)
    cv2.imshow('Warped', imgOut)
    
    # If any button is pressed, destroy all windows and exit program
    cv2.waitKey(0)
    cv2.destroyAllWindows()