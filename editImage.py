import cv2
import numpy as np
from stackImages import StackImages


## The specified image path.
path = 'Floss/p04.png'
path = 'Resources/donuts.jpg'

## The original image.
img = cv2.imread(path)

## The original image after applying grayscale.
imgGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

## The grayscale image after applying gaussian blur.
imgBlur = cv2.GaussianBlur(imgGray, (7,7), 0)

## The blurred image after applying Canny edge detection
imgCanny = cv2.Canny(img, 100, 200)

## The kernel of the dilated image processor.
kernel = np.ones((5, 5), np.uint8)
## The Canny edge-detected image after dilating the edges.
imgDilation = cv2.dilate(imgCanny, kernel, iterations=1)

## The dilated image after eroding the edges.
imgEroded = cv2.erode(imgDilation, kernel, iterations=1)

## An array of the processed images.
results = [[img, imgGray, imgBlur], [imgCanny, imgDilation, imgEroded]]

# Show results
cv2.imshow("Result", StackImages(0.5, results))


# If any button is pressed, destroy all windows and exit program
cv2.waitKey(0)
cv2.destroyAllWindows()
