import cv2
import numpy as np

## An image file containing a black window with dimensions 512x512
img = np.zeros((512, 512, 3), np.uint8)

# Draw a green line from the top-left to the bottom-left of the screen
cv2.line(img, (0,0), (img.shape[1], img.shape[0]), (0, 255, 0), 3)

# Draw a red rectantle, starting in the top-left, with dimensions 250x300
cv2.rectangle(img, (0,0), (250,300), (0, 0, 255), 2)

# Draw a cyan circle at point (400, 50) with a radius of 30 pixels.
cv2.circle(img, (400, 50), 30, (255, 255, 0), 5)

# Draw a dark green string of text, titled "OpenCV" starting at point (300,300) with size 1 font.
cv2.putText(img, 'OpenCV', (300, 200), cv2.FONT_HERSHEY_COMPLEX, 1, (0,150,0), 1)

# Show the image
cv2.imshow('Image', img)

# If any button is pressed, destroy all windows and exit program
cv2.waitKey(0)
cv2.destroyAllWindows()