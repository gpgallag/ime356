'''
@file Project.py

@brief Normalizes the orientation of an assymetric (odd) shaped image.

@author Grant Gallagher

@Copyright Copyright Grant Gallagher, All rights reserved 2021.
'''


import cv2
import numpy as np
import os
import time

def ImportFolder(folder_name):
    '''
    @brief               Imports the contents of a specified folder.
    @param folder_name   String: The specified path of the folder.
    @return images       List:   A list of image files containing the image data within the folder.
    @return image_names: List:   A list of strings containing the image names within the folder.
    '''
    images = []
    image_names = []
    for file_name in os.listdir(folder_name):
        img = cv2.imread(os.path.join(folder_name, file_name))
        if img is not None:
            images.append(img)
            image_names.append(file_name)
    return (images, image_names)


def StackImages(scale, imgArray):
    '''
    @brief          Exports the a set of images as a single image in row-column form.
    @param scale    Float: The sizing scale of the outputted images, with respect to the original images.
    @param imgArray List: An array of images to be exported in row-column order.
    @return ver     Image: An image file containing the set of specfied images as one file.
    '''
    rows = len(imgArray)
    cols = len(imgArray[0])
    rowsAvailable = isinstance(imgArray[0], list)
    width = imgArray[0][0].shape[1]
    height = imgArray[0][0].shape[0]
    if rowsAvailable:
        for x in range ( 0, rows):
            for y in range(0, cols):
                if imgArray[x][y].shape[:2] == imgArray[0][0].shape [:2]:
                    imgArray[x][y] = cv2.resize(imgArray[x][y], (0, 0), None, scale, scale)
                else:
                    imgArray[x][y] = cv2.resize(imgArray[x][y], (imgArray[0][0].shape[1], imgArray[0][0].shape[0]), None, scale, scale)
                if len(imgArray[x][y].shape) == 2: imgArray[x][y]= cv2.cvtColor( imgArray[x][y], cv2.COLOR_GRAY2BGR)
        imageBlank = np.zeros((height, width, 3), np.uint8)
        hor = [imageBlank]*rows
        for x in range(0, rows):
            hor[x] = np.hstack(imgArray[x])
        ver = np.vstack(hor)
    else:
        for x in range(0, rows):
            if imgArray[x].shape[:2] == imgArray[0].shape[:2]:
                imgArray[x] = cv2.resize(imgArray[x], (0, 0), None, scale, scale)
            else:
                imgArray[x] = cv2.resize(imgArray[x], (imgArray[0].shape[1], imgArray[0].shape[0]), None,scale, scale)
            if len(imgArray[x].shape) == 2: imgArray[x] = cv2.cvtColor(imgArray[x], cv2.COLOR_GRAY2BGR)
        hor= np.hstack(imgArray)
        ver = hor
    return ver


def Preprocess(img, lowerb, upperb):
    '''
    @brief Processes an image into binary form.
    @param img Image: An image file that is to be converted into binary form.
    @param lowerb List: A list of the lower bound thresholds of the Hue, Saturation, and Value
    @param upperb List: A list of the upper bound thresholds of the Hue, Saturation, and Value.
    @return img  Image: An image file containing the post-processed image, converted to binary form.
    '''
    lowerb = np.array(lowerb)
    upperb = np.array(upperb)
    img = cv2.inRange(img, lowerb, upperb)
    img = cv2.erode(img, None, iterations=1)
    img = cv2.dilate(img, None, iterations=6)
    img = cv2.erode(img, None, iterations=1)
    # imgStack = stackImages(0.8, ([image, mask, thresh]))
    return img


def Centroid(img):
    '''
    @brief Calculates the geometric centroid of an image.
    @param img Image: A binary image file.
    @return cx Int: An integer representing the centroid of the image along the x-axis.
    @return cy Int: An integer representing the centroid of the image along the y-axis.
    '''
    contours,hierarchy = cv2.findContours(img, 1, 2)
    cnt = contours[0]
    M = cv2.moments(cnt)
    cx = int(M['m10']/M['m00'])
    cy = int(M['m01']/M['m00'])
    return (cx, cy)


def Box(img):
    '''
    @brief Calculates the minimum area rectangle that encloses a binary image contour.
    @param img Image A binary image file.
    @return box Tuple: A Box2D structure containing the following details -
                       ( top-left corner(x,y), (width, height), angle of rotation ). 
    '''
    contours,hierarchy = cv2.findContours(img, 1, 2)
    cnt = contours[0]
    rect = cv2.minAreaRect(cnt)
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    return box


def BoxCenter(box):
    '''
    @brief Calculates the centerpoint of a Box2D structure object
    @param box Tuple: A Box2D structure object.
    @return x The centerpoint location of a box structure along the x-axis.
    @return y The centerpoint location of a box structure along the y-axis.
    '''
    sumx = 0
    sumy = 0
    for index in range(len(box)):
        sumx = sumx + box[[index], [0]]
        sumy = sumy + box[[index], [1]]

    x = int(sumx/4)
    y = int(sumy/4)
    return (x, y)


def NormalizeOrientation(img, angle):
    '''
    @brief Rotates an image and expands the image bounds to avoid cropping.
    @param img Image: An image file.
    @param angle Float: The specified angle, in degrees, to rotate an image clockwise.
    @return rotated_image Image: A rotated image file.
    '''

    height, width = img.shape[:2] # image shape has 3 dimensions
    image_center = (width/2, height/2) # getRotationMatrix2D needs coordinates in reverse order (width, height) compared to shape

    rotation_img = cv2.getRotationMatrix2D(image_center, angle, 1.)

    # rotation calculates the cos and sin, taking absolutes of those.
    abs_cos = abs(rotation_img[0,0]) 
    abs_sin = abs(rotation_img[0,1])

    # find the new width and height bounds
    bound_w = int(height * abs_sin + width * abs_cos)
    bound_h = int(height * abs_cos + width * abs_sin)

    # subtract old image center (bringing image back to origo) and adding the new image center coordinates
    rotation_img[0, 2] += bound_w/2 - image_center[0]
    rotation_img[1, 2] += bound_h/2 - image_center[1]

    # rotate image with the new bounds and translated rotation imgrix
    rotated_img = cv2.warpAffine(img, rotation_img, (bound_w, bound_h))
    return rotated_img   

def Standardize(img, thresh):
    '''
    @brief Orients a specified image in a normal manner, depending on the location of its centroid and centerpoint.
    @param img Image: An Image file.
    @param thresh Image: The respective binary version of the img file.
    @return result Image: An Image file orientented such that its centroid is in the top-right quadrant. 
    '''
    # Find new centroid and rect of rotated image
    centroid = Centroid(thresh)
    contours,hierarchy = cv2.findContours(thresh_rotated, 1, 2)
    cnt = contours[0]
    
    # Find smallest box
    rect = cv2.minAreaRect(cnt)
    box_corners = cv2.boxPoints(rect)
    box_corners = np.int0(box_corners)
    box_center = BoxCenter(box_corners)
    
    x_dist = centroid[0] - box_center[0]
    y_dist = centroid[1] - box_center[1]
    
    #cv2.imshow("Rotated", image_rotated)
    # TL Quadrant - RH
    if x_dist<0 and y_dist<0 and abs(x_dist)>abs(y_dist):
        result = cv2.transpose(image_rotated)
        result = cv2.flip(result, 1)
    # TL Quadrant - LH
    elif x_dist<0 and y_dist<0 and abs(x_dist)<abs(y_dist):
        result = cv2.flip(image_rotated, 1)
    # BL Quadrant - RH
    elif x_dist<0 and y_dist>0 and abs(x_dist)<abs(y_dist):
        result = cv2.flip(image_rotated, -1)
    # BL Quadrant - LH
    elif x_dist<0 and y_dist>0 and abs(x_dist)>abs(y_dist):
        result = cv2.transpose(image_rotated)
        result = cv2.flip(result, 1)
        result = cv2.flip(result, 1)
    # BR Quadrant - RH
    elif x_dist>0 and y_dist>0 and abs(x_dist)>abs(y_dist):
        result = cv2.transpose(image_rotated)
        result = cv2.flip(result, 1)
        result = cv2.flip(result, 1)
    # BR Quadrant - LH
    elif x_dist>0 and y_dist>0 and abs(x_dist)<abs(y_dist):
        result = cv2.flip(image_rotated, 0)
    # TR Quadrant - RH
    elif x_dist>0 and y_dist<0 and abs(x_dist)<abs(y_dist):
        result = image_rotated
        pass
    # TR Quadrant - LH
    elif x_dist>0 and y_dist<0 and abs(x_dist)>abs(y_dist):
        result = cv2.transpose(image_rotated)
        result = cv2.flip(result, 1)
        result = cv2.flip(result, 0)
    else:
        result = image_rotated
    return result
        
        
if __name__ == '__main__':
    debug = True
    
    print('Welcome')
    print('The Left and Right arrow keys are used to parse through the folder of images')
    print('The ESC key is used to exit the program and close all image windows.')
    print('Debug state is ' + str(debug))
    
    ## Pathway of Image
    folder_contents = ImportFolder('Floss')
    images = folder_contents[0]
    image_names = folder_contents[1]
    index = 0
    while True:
        start_time = time.time()
        # - - - - - - - - - - Import and Threshold the Image - - - - - - - - - - 
        ## The original image in full-color format.
        original = images[index]
        
        ## The lower bounds of the image mask [Hue, Sat, Val].
        lowerb = [50, 60, 0]
        ## The upper bounds of the image mask [Hue, Sat, Val]
        upperb = [175, 256, 135]
        ## A thresholded copy of the original image.
        thresh = Preprocess(original, lowerb, upperb)
        
        
        
        # - - - - - - - - - - Find the Smallest Enclosing Box and Reorient - - -
        # Find contours
        contours, hierarchy = cv2.findContours(thresh, 1, 2)
        cnt = contours[0]
        
        # Find smallest box
        rect = cv2.minAreaRect(cnt)
        # Find angle of rotation
        rect_angle = rect[2]
        
        # Rotate so that the rectangle is orthoganal
        image_rotated = NormalizeOrientation(original, rect_angle)
        thresh_rotated = Preprocess(image_rotated, lowerb, upperb)
        
        # Standardize
        result = Standardize(image_rotated, thresh_rotated)
        result_thresh = Preprocess(result, lowerb, upperb)
    
    
        # - - - - - - - - - - Crop, Scale, and Export the Images - - - - - - - - - - 
        # Find new contours
        contours,hierarchy = cv2.findContours(result_thresh, 1, 2)
        cnt = contours[0]
        # Find smallest box
        x,y,w,h = cv2.boundingRect(cnt)
        # Cropped image
        final = result[y:(y+h), x:(x+w)]
            
        # - - - - - - - - - - - - - Display - - - - - - - - - - - - -
        # cv2.imshow("Originals", StackImages(1, [original, thresh]))
        # cv2.imshow("Normalized", StackImages(1, [image_rotated, thresh_rotated]))
        if debug == False:
            cv2.imshow(str(image_names[index])+' - Original', images[index])
            cv2.imshow(str(image_names[index])+' - Result', final)
            
        elif debug == True:
            #Markup of original Image
            original_markup = original.copy()
            cv2.circle(original_markup, (Centroid(thresh)), 3, (0, 255, 0), 5)
            # cv2.circle(original_markup, (int(original_markup.shape[1]/2), int(original_markup.shape[0]/2)), 3, (255, 255, 255), 5)
            cv2.drawContours(original_markup, [Box(thresh)], 0, (0, 255, 0), 2)
            
            # Markup of rotated Image
            image_rotated_markup = image_rotated.copy()
            cv2.circle(image_rotated_markup, (Centroid(thresh_rotated)), 3, (0, 255, 0), 5)           
            # cv2.circle(image_rotated_markup, BoxCenter(Box(thresh_rotated)), 3, (255, 255, 255), 5)
            cv2.drawContours(image_rotated_markup, [Box(thresh_rotated)], 0, (0, 255, 0), 2)
            
            # Markup of standardized Image
            result_markup = result.copy()
            cv2.circle(result_markup, (Centroid(result_thresh)), 3, (0, 255, 0), 5)
            cv2.circle(result_markup, BoxCenter(Box(result_thresh)), 3, (255, 255, 255), 5)
            cv2.drawContours(result_markup, [Box(result_thresh)], 0, (0, 255, 0), 2)
            
            
            processing_steps = [[original, thresh, original_markup], [image_rotated, thresh_rotated, image_rotated_markup], [result, result_thresh, result_markup]]
            
            cv2.imshow(str(image_names[index])+' - Processing', StackImages(.55, processing_steps))
            cv2.imshow(str(image_names[index])+' - Result', final)
            
        else:
            pass
        
        
        end_time = time.time()
        print('Results generated in ' + str(int((end_time - start_time)*1e2)) + ' ms')
        # - - - - - - - - - - - - - Do stuff in here - - - - - - - - - - - -
        key = cv2.waitKeyEx(0)
        
        if key == 2555904:
            cv2.destroyAllWindows()
            if index == len(images) - 1:
                index = 0
            else:
                index += 1
        elif key == 2424832:
            cv2.destroyAllWindows()
            if index == 0:
                index = len(images) - 1
            else:
                index -= 1
        elif key == 27:
            cv2.destroyAllWindows()
            break
        else:
            pass
