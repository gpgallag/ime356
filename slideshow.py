import cv2
import os


def ImportFolder(folder_name):
    '''
    @brief               Imports the contents of a specified folder.
    @param folder_name   String: The specified path of the folder.
    @return images       List:   A list of image files containing the image data within the folder.
    @return image_names: List:   A list of strings containing the image names within the folder.
    '''
    images = []
    image_names = []
    for file_name in os.listdir(folder_name):
        img = cv2.imread(os.path.join(folder_name, file_name))
        if img is not None:
            images.append(img)
            image_names.append(file_name)
    return (images, image_names)

## Pathway of Image
folder_contents = ImportFolder('Floss')
images = folder_contents[0]
image_names = folder_contents[1]
index = 0
while True:
    cv2.imshow(str(image_names[index])+' - Original', images[index])
    
    key = cv2.waitKeyEx(0)
    if key == 2555904:
        cv2.destroyAllWindows()
        if index == len(images) - 1:
            index = 0
        else:
            index += 1
    elif key == 2424832:
        cv2.destroyAllWindows()
        if index == 0:
            index = len(images) - 1
        else:
            index -= 1
    elif key == 27:
        cv2.destroyAllWindows()
        break
    else:
        pass