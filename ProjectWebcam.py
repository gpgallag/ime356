'''
@file Project.py

@brief Normalizes the orientation of an assymetric (odd) shaped image.

@author Grant Gallagher

@Copyright Copyright Grant Gallagher, All rights reserved 2021.
'''


import cv2
import numpy as np
import os
import time

def ImportFolder(folder_name):
    '''
    @brief               Imports the contents of a specified folder.
    @param folder_name   String: The specified path of the folder.
    @return images       List:   A list of image files containing the image data within the folder.
    @return image_names: List:   A list of strings containing the image names within the folder.
    '''
    images = []
    image_names = []
    for file_name in os.listdir(folder_name):
        img = cv2.imread(os.path.join(folder_name, file_name))
        if img is not None:
            images.append(img)
            image_names.append(file_name)
    return (images, image_names)


def StackImages(scale, imgArray):
    '''
    @brief          Exports the a set of images as a single image in row-column form.
    @param scale    Float: The sizing scale of the outputted images, with respect to the original images.
    @param imgArray List: An array of images to be exported in row-column order.
    @return ver     Image: An image file containing the set of specfied images as one file.
    '''
    rows = len(imgArray)
    cols = len(imgArray[0])
    rowsAvailable = isinstance(imgArray[0], list)
    width = imgArray[0][0].shape[1]
    height = imgArray[0][0].shape[0]
    if rowsAvailable:
        for x in range ( 0, rows):
            for y in range(0, cols):
                if imgArray[x][y].shape[:2] == imgArray[0][0].shape [:2]:
                    imgArray[x][y] = cv2.resize(imgArray[x][y], (0, 0), None, scale, scale)
                else:
                    imgArray[x][y] = cv2.resize(imgArray[x][y], (imgArray[0][0].shape[1], imgArray[0][0].shape[0]), None, scale, scale)
                if len(imgArray[x][y].shape) == 2: imgArray[x][y]= cv2.cvtColor( imgArray[x][y], cv2.COLOR_GRAY2BGR)
        imageBlank = np.zeros((height, width, 3), np.uint8)
        hor = [imageBlank]*rows
        for x in range(0, rows):
            hor[x] = np.hstack(imgArray[x])
        ver = np.vstack(hor)
    else:
        for x in range(0, rows):
            if imgArray[x].shape[:2] == imgArray[0].shape[:2]:
                imgArray[x] = cv2.resize(imgArray[x], (0, 0), None, scale, scale)
            else:
                imgArray[x] = cv2.resize(imgArray[x], (imgArray[0].shape[1], imgArray[0].shape[0]), None,scale, scale)
            if len(imgArray[x].shape) == 2: imgArray[x] = cv2.cvtColor(imgArray[x], cv2.COLOR_GRAY2BGR)
        hor= np.hstack(imgArray)
        ver = hor
    return ver


def Preprocess(img, lowerb, upperb):
    '''
    @brief Processes an image into binary form.
    @param img Image: An image file that is to be converted into binary form.
    @param lowerb List: A list of the lower bound thresholds of the Hue, Saturation, and Value
    @param upperb List: A list of the upper bound thresholds of the Hue, Saturation, and Value.
    @return img  Image: An image file containing the post-processed image, converted to binary form.
    '''
    lowerb = np.array(lowerb)
    upperb = np.array(upperb)
    img = cv2.inRange(img, lowerb, upperb)
    img = cv2.erode(img, None, iterations=1)
    img = cv2.dilate(img, None, iterations=6)
    img = cv2.erode(img, None, iterations=1)
    # imgStack = stackImages(0.8, ([image, mask, thresh]))
    return img


def Centroid(img):
    '''
    @brief Calculates the geometric centroid of an image.
    @param img Image: A binary image file.
    @return cx Int: An integer representing the centroid of the image along the x-axis.
    @return cy Int: An integer representing the centroid of the image along the y-axis.
    '''
    contours,hierarchy = cv2.findContours(img, 1, 2)
    cnt = contours[0]
    M = cv2.moments(cnt)
    cx = int(M['m10']/M['m00'])
    cy = int(M['m01']/M['m00'])
    return (cx, cy)


def Box(img):
    '''
    @brief Calculates the minimum area rectangle that encloses a binary image contour.
    @param img Image A binary image file.
    @return box Tuple: A Box2D structure containing the following details -
                       ( top-left corner(x,y), (width, height), angle of rotation ). 
    '''
    contours,hierarchy = cv2.findContours(img, 1, 2)
    cnt = contours[0]
    rect = cv2.minAreaRect(cnt)
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    return box


def BoxCenter(box):
    '''
    @brief Calculates the centerpoint of a Box2D structure object
    @param box Tuple: A Box2D structure object.
    @return x The centerpoint location of a box structure along the x-axis.
    @return y The centerpoint location of a box structure along the y-axis.
    '''
    sumx = 0
    sumy = 0
    for index in range(len(box)):
        sumx = sumx + box[[index], [0]]
        sumy = sumy + box[[index], [1]]

    x = int(sumx/4)
    y = int(sumy/4)
    return (x, y)


def NormalizeOrientation(img, angle):
    '''
    @brief Rotates an image and expands the image bounds to avoid cropping.
    @param img Image: An image file.
    @param angle Float: The specified angle, in degrees, to rotate an image clockwise.
    @return rotated_image Image: A rotated image file.
    '''

    height, width = img.shape[:2] # image shape has 3 dimensions
    image_center = (width/2, height/2) # getRotationMatrix2D needs coordinates in reverse order (width, height) compared to shape

    rotation_img = cv2.getRotationMatrix2D(image_center, angle, 1.)

    # rotation calculates the cos and sin, taking absolutes of those.
    abs_cos = abs(rotation_img[0,0]) 
    abs_sin = abs(rotation_img[0,1])

    # find the new width and height bounds
    bound_w = int(height * abs_sin + width * abs_cos)
    bound_h = int(height * abs_cos + width * abs_sin)

    # subtract old image center (bringing image back to origo) and adding the new image center coordinates
    rotation_img[0, 2] += bound_w/2 - image_center[0]
    rotation_img[1, 2] += bound_h/2 - image_center[1]

    # rotate image with the new bounds and translated rotation imgrix
    rotated_img = cv2.warpAffine(img, rotation_img, (bound_w, bound_h))
    return rotated_img   

def Standardize(img, thresh):
    '''
    @brief Orients a specified image in a normal manner, depending on the location of its centroid and centerpoint.
    @param img Image: An Image file.
    @param thresh Image: The respective binary version of the img file.
    @return result Image: An Image file orientented such that its centroid is in the top-right quadrant. 
    '''
    # Find new centroid and rect of rotated image
    centroid = Centroid(thresh)
    contours,hierarchy = cv2.findContours(thresh_rotated, 1, 2)
    cnt = contours[0]
    
    # Find smallest box
    rect = cv2.minAreaRect(cnt)
    box_corners = cv2.boxPoints(rect)
    box_corners = np.int0(box_corners)
    box_center = BoxCenter(box_corners)
    
    x_dist = centroid[0] - box_center[0]
    y_dist = centroid[1] - box_center[1]
    
    #cv2.imshow("Rotated", image_rotated)
    # TL Quadrant - RH
    if x_dist<0 and y_dist<0 and abs(x_dist)>abs(y_dist):
        result = cv2.transpose(image_rotated)
        result = cv2.flip(result, 1)
    # TL Quadrant - LH
    elif x_dist<0 and y_dist<0 and abs(x_dist)<abs(y_dist):
        result = cv2.flip(image_rotated, 1)
    # BL Quadrant - RH
    elif x_dist<0 and y_dist>0 and abs(x_dist)<abs(y_dist):
        result = cv2.flip(image_rotated, -1)
    # BL Quadrant - LH
    elif x_dist<0 and y_dist>0 and abs(x_dist)>abs(y_dist):
        result = cv2.transpose(image_rotated)
        result = cv2.flip(result, 1)
        result = cv2.flip(result, 1)
    # BR Quadrant - RH
    elif x_dist>0 and y_dist>0 and abs(x_dist)>abs(y_dist):
        result = cv2.transpose(image_rotated)
        result = cv2.flip(result, 1)
        result = cv2.flip(result, 1)
    # BR Quadrant - LH
    elif x_dist>0 and y_dist>0 and abs(x_dist)<abs(y_dist):
        result = cv2.flip(image_rotated, 0)
    # TR Quadrant - RH
    elif x_dist>0 and y_dist<0 and abs(x_dist)<abs(y_dist):
        result = image_rotated
        pass
    # TR Quadrant - LH
    elif x_dist>0 and y_dist<0 and abs(x_dist)>abs(y_dist):
        result = cv2.transpose(image_rotated)
        result = cv2.flip(result, 1)
        result = cv2.flip(result, 0)
    else:
        result = image_rotated
    return result

def Empty(a):
    '''
    @brief An Empty function used to create the taskbar.
    '''
    pass
        
if __name__ == '__main__':   
    print('Welcome')
    
    ## Pathway of Image
    cap = cv2.VideoCapture(0, cv2.CAP_DSHOW)
    cap.set(3,640)
    cap.set(4,480)
    frameRate = 24
    
    
    # Create Trackbar sliders used to control the thresholds of Hue, Saturation, and Value.
    cv2.namedWindow('TrackBars')
    cv2.resizeWindow('TrackBars', 640, 280)

    cv2.createTrackbar('Hue Min', 'TrackBars', 0,   180, Empty)
    cv2.createTrackbar('Hue Max', 'TrackBars', 180, 180, Empty)
    cv2.createTrackbar('Sat Min', 'TrackBars', 0, 256, Empty)
    cv2.createTrackbar('Sat Max', 'TrackBars', 256, 256, Empty)
    cv2.createTrackbar('Val Min', 'TrackBars', 0,   256, Empty)
    cv2.createTrackbar('Val Max', 'TrackBars', 256,   256, Empty)
    time.sleep
    
    run = True
    while run == True:
        # - - - - - - - - - - Import and Threshold the Image - - - - - - - - - - 
        ## The original image in full-color format.
        ## Taking one frame from the webcam
        success, original = cap.read()
        

        ## The minimum hue threshold.
        h_min = cv2.getTrackbarPos('Hue Min', 'TrackBars')    
        ## The maximim hue threshold.
        h_max = cv2.getTrackbarPos('Hue Max', 'TrackBars')    
        ## The minimum saturation threshold.
        s_min = cv2.getTrackbarPos('Sat Min', 'TrackBars')    
        ## The maximum saturation threshold.
        s_max = cv2.getTrackbarPos('Sat Max', 'TrackBars')    
        ## The minimum value threshold.
        v_min = cv2.getTrackbarPos('Val Min', 'TrackBars')
        ## The maximum value threshold.
        v_max = cv2.getTrackbarPos('Val Max', 'TrackBars')
    
    
        ## A list containing the lower bound threshold values.
        lowerb = np.array([h_min, s_min, v_min])
        ## A list containing the upper bound threshold values.
        upperb = np.array([h_max, s_max, v_max])
        ## A thresholded copy of the original image.
        thresh = Preprocess(original, lowerb, upperb)
        
        
        
        # - - - - - - - - - - Find the Smallest Enclosing Box and Reorient - - -
        try:
            # Find contours
            contours, hierarchy = cv2.findContours(thresh, 1, 2)
            cnt = contours[0]
            
            # Find smallest box
            rect = cv2.minAreaRect(cnt)
            # Find angle of rotation
            rect_angle = rect[2]
            
            # Rotate so that the rectangle is orthoganal
            image_rotated = NormalizeOrientation(original, rect_angle)
            thresh_rotated = Preprocess(image_rotated, lowerb, upperb)
            
            # Standardize
            result = Standardize(image_rotated, thresh_rotated)
            result_thresh = Preprocess(result, lowerb, upperb)
        
        
            # - - - - - - - - - - Crop, Scale, and Export the Images - - - - - - - - - - 
            # Find new contours
            contours,hierarchy = cv2.findContours(result_thresh, 1, 2)
            cnt = contours[0]
            # Find smallest box
            x,y,w,h = cv2.boundingRect(cnt)
            # Cropped image
            final = result[y:(y+h), x:(x+w)]
                
            # - - - - - - - - - - - - - Display - - - - - - - - - - - - -
            raw = StackImages(1, ([original, thresh]))
            cv2.imshow('Raw', raw)
            cv2.imshow('Result', final)
            
            # - - - - - - - - - - - - - Do stuff in here - - - - - - - - - - - -
            key = cv2.waitKeyEx(1)
            
            if key != -1:
                cv2.destroyAllWindows()
                run = False
            
            else:
                time.sleep(1/frameRate)
                pass
        except:
            print('Cannot find the counter of the image.')
            key = cv2.waitKeyEx(1)
            
            if key != -1:
                cv2.destroyAllWindows()
                run = False
            
            else:
                time.sleep(1/frameRate)
                pass

