## @file mainpage.py
#  Brief doc for mainpage.py
#  
#  Detailed doc for mainpage.py 
#  
#  @mainpage
#
#  
#  @section sec_intro Introduction
#  Welcome! This html website serves as documentation for the code developed for
#  the term project of IME 356: Manufacturing Automation - taught by Dr. Jose Macedo.
#  
#  @section sec_details Term Project Scripts
#  The following sections are links to the individual python scripts that were developed and used while learning OpenCV - leading up to
#  the culminations of the term project, which is a script that definines a coordinate system for object image and standardizes the orientation of the object.
#  See individual modules for details. \n \n
#  \b Modules:
#  - <a href="https://bitbucket.org/gpgallag/ime356/src/master/importImage.py"> <b>Importing an Image</b></a>
#  - <a href="https://bitbucket.org/gpgallag/ime356/src/master/importWebcam.py"> <b>Importing a Webcam</b></a>
#  - <a href="https://bitbucket.org/gpgallag/ime356/src/master/editImage.py"> <b>Basic Image Processing</b></a>
#  - <a href="https://bitbucket.org/gpgallag/ime356/src/master/filterImageColor.py"> <b>Image Masking</b></a>
#  - <a href="https://bitbucket.org/gpgallag/ime356/src/master/filterWebcamColor.py"> <b>Video Masking</b></a>
#  - <a href="https://bitbucket.org/gpgallag/ime356/src/master/sizeImage.py"> <b>Sizing and Scaling Images</b></a>
#  - <a href="https://bitbucket.org/gpgallag/ime356/src/master/drawingShapes.py"> <b>Drawing Shapes</b></a>
#  - <a href="https://bitbucket.org/gpgallag/ime356/src/master/warpImage.py"> <b>Affine Transformations</b></a>
#  - <a href="https://bitbucket.org/gpgallag/ime356/src/master/objectMeasurement.py"> <b>Realtime Object Measurement</b></a>
#  - <a href="https://bitbucket.org/gpgallag/ime356/src/master/licensePlateDetector.py"> <b>Haar Cascade Pattern Matching License Plates</b></a>
#  - <a href="https://bitbucket.org/gpgallag/ime356/src/master/Project.py"> <b>*Term Project: Defining a Coordinate System </b></a>
#  - <a href="https://bitbucket.org/gpgallag/ime356/src/master/ProjectWebcam.py"> <b>*Term Project: Defining a Coordinate System in Real-time</b></a>
#  .
#  Subsequently, the documentation for the term project python file can be found in the navigation tree under @ref Project.py
#
#  @image html logos.png width=400cm
#
#  - - -
#  @section sec1 What is OpenCV?
#  OpenCV is the huge open-source library for the computer vision, machine learning,
#  and image processing and now it plays a major role in real-time operation which is
#  very important in today’s systems. By using it, one can process images and videos to
#  identify objects, faces, or even handwriting of a human. When it integrated with various libraries,
#  such as Numpy, python is capable of processing the OpenCV array structure for analysis.
#  To identify image pattern and its various features, we use vector space and perform
#  mathematical operations on these features. The first OpenCV version was 1.0. OpenCV is released under a BSD license
#  and hence it’s free for both academic and commercial use. It has C++, C, Python
#  and Java interfaces and supports Windows, Linux, Mac OS, iOS and Android. When OpenCV
#  was designed the main focus was real-time applications for computational efficiency.
#  All things are written in optimized C/C++ to take advantage of multi-core processing. \n \n
#
#  With the huge spike in processing hardware and technology, computer vision has
#  gained significant traction in industrial and commercial application.ome of them are listed below:
#  - Facial recognition
#  - Automated inspection and surveillance
#  - Vehicle counting on highways along with their speeds
#  - Anamoly (defect) detection in the manufacturing process (the odd defective products)
#  - Street view image stitching
#  - Object recognition
#  - Medical image analysis
#  - Movies – 3D structure from motion
#
#  @section sec2 Image Processing
#  Image processing is a method to perform some operations on an image, in order to
#  get an enhanced image and or to extract some useful information from it.
#  If we talk about the basic definition of image processing then
#  Image processing is the analysis and manipulation of a digitized image,
#  especially in order to improve its quality.
#  An image may be defined as a two-dimensional function f(x, y), where x and y
#  are spatial(plane) coordinates, and the amplitude of f at any pair of coordinates
#  (x, y) is called the intensity or grey level of the image at that point.
#  In another word An image is nothing more than a two-dimensional matrix (3-D in case of coloured images)
#  which is defined by the mathematical function f(x, y) at any point is giving the
#  pixel value at that point of an image, the pixel value describes how bright
#  that pixel is, and what colour it should be.
#  Image processing is basically signal processing in which input is an image and
#  output is image or characteristics according to requirement associated with that image.
#  Image processing basically includes the following three steps:
#  - Importing the image
#  - Analysing and manipulating the image
#  - Output in which result can be altered image or report that is based on image analysis
#
#  - - - 
#
#  @section sec3 Getting Started
#  <b>Installing Anaconda and Python</b> \n
#  In order to use Python on your PC you will need to install a Python IDE Integrated Development Environment) or compiler.
#  You may use the IDE of your choice, such as VScode, however I will demonstrate how to install Anaconda and Spyder:\n
#  1. Go to *https://www.anaconda.com/distribution/*
#  2. Click the download link on the Anaconda website, then select the installer applicable for your operating system.
#  3. Select the *Individual Edition* option when you download the installer.
#  4. Run the installer on your PC, following all prompts that appear during the installation process.\n \n
#  .
#
#  <b>Installing OpenCV</b> \n
#  OpenCV is an open-source computer-vision library that is available to multiple programming languages,
#  including Python, C++, and Java. It is also available on multiple platforms: Windows, Linux, macOS.\n
#  1. Go to *https://pypi.org/project/opencv-python*
#  2. Following the Installation and Usage, select the appropriate OS/environment for your application.
#  3. Install the appropriate package on your PC. (In most cases, open your command prompt and run the following line of code: pip install opencv-Python)
#  .
#  With Anaconda/Spyder installed, you should be able to open up Spyder to a python terminal. The left-hand side is the console - which is where you will write
#  your code. The right-hand side is your kernel. Here is where you will find all of the variable descriptions, plots, and outputs relating to your scripts.
#  To use OpenCV in python, simply start every script with the command <b>import cv2</b>. Since OpenCV utilizes numpy, it is also
#  very common to include <b>import numpy</b> as well - this way you can structure matrices more easily.
#
#  - - -
#
#  @section sec4 Additional Information
#  - <b>Additional information about OpenCV:</b> *https://opencv.org/*
#  - <b>OpenCV Library Documentation:</b> *https//docs.opencv.org/*
#  - <b>OpenCV Tutorials:</b> https://docs.opencv.org/master/d9/df8/tutorial_root.html*
#
#
#  @section respository Source Code Repository
#  Here is a complete repository containing the source code of all of the scripts developed, as well as the image files:
#  *https://bitbucket.org/gpgallag/ime356/src/master/* \n \n
#  - - -
#  @author Grant Gallagher
# 
#  @copyright Copyright © 2021 Grant Gallagher, all rights reserved.